import os
import re
from typing import List, Tuple

import spacy

_MODEL_PATH = os.path.join('/opt/ml/', 'model')  # Path where all your model(s) live in

BLACKLISTED_CHARS_IN_RESULT = ["+", ":", "<", ">", "/", "*", "=", "%", "&"]
RE_MULTIPLE_WHITESPACES_BETWEEN_DIGITS = r'(\d)\s+(\d)'
CUSTOM_CURRENCY_SYMBOLS = ["₣", "$", "€", "£", "¥", "₹", "د.ك"]
CUSTOM_CURRENCY_CODES = ['aed', 'afn', 'amd', 'ang', 'aoa', 'ars', 'aud', 'awg', 'afl', 'azn', 'bam', 'bdt', 'bbd', 'bgn', 'bhd', 'bif', 'bsd', 'bmd', 'bnd', 'bob', 'bov', 'brl', 'btn', 'bwp', 'byn', 'bzd', 'cad', 'cdf', 'che', 'chf', 'chw', 'clf', 'clp', 'cny', 'cop', 'cou', 'crc', 'cuc', 'cup', 'cve', 'czk', 'djf', 'dkk', 'dop', 'dzd', 'eek', 'egp', 'ern', 'etb', 'eur', 'fjd', 'fkp', 'gbp', 'gel', 'ggp', 'ghs', 'gip', 'gmd', 'gnf', 'gtq', 'gyd', 'hkd', 'hnl', 'hrk', 'htg', 'huf', 'idr', 'ils', 'imp', 'inr', 'iqd', 'irr', 'isk', 'jep', 'jmd', 'jod', 'jpy', 'kes', 'kgs', 'khr', 'kmf', 'kpw', 'krw', 'kwd', 'kyd', 'kzt', 'lak', 'lbp', 'lkr', 'lrd', 'lsl', 'ltl', 'lvl', 'lyd', 'mad', 'mdl', 'mga', 'mkd', 'mmk', 'mnt', 'mop', 'mro', 'mur', 'mvr', 'mwk', 'mxn', 'mxv', 'myr', 'mzn', 'nad', 'ngn', 'nio', 'nok', 'npr', 'prb', 'nzd', 'omr', 'pab', 'pen', 'pgk', 'php', 'pkr', 'pln', 'pyg', 'qar', 'ron', 'rsd', 'rub', 'rwf', 'sar', 'sbd', 'scr', 'sdg', 'sek', 'sgd', 'shp', 'sll', 'sos', 'srd', 'ssp', 'std', 'svc', 'syp', 'szl', 'thb', 'tjs', 'tmt', 'tnd', 'try', 'ttd', 'tvd', 'twd', 'tzs', 'uah', 'ugx', 'usd', 'usn', 'uyi', 'uyu', 'uzs', 'vef', 'vnd', 'vuv', 'wst', 'xaf', 'xag', 'xau', 'xba', 'xbb', 'xbc', 'xbd', 'xcd', 'xdr', 'xof', 'xpd', 'xpf', 'xpt', 'xsu', 'xts', 'xua', 'xxx', 'yer', 'zar', 'zmk', 'zmw', 'zwd', 'zwl', 'ntd', 'rmb']


def clean_str(input_string: str) -> str:
    str_en = input_string.encode("ascii", "ignore")
    str_de = str_en.decode()
    invalid_chars = ['—', '-', '\'', ',', '’', '–', '"', '|', '_', '`', '[', ']']
    for char in invalid_chars:
        str_de = str_de.replace(char, '')
    return ' '.join(str_de.split())


def preprocess_input_text(input_text: str) -> str:
    input_text = clean_str(input_text)
    input_text = input_text.lower()
    input_text = " ".join(input_text.split())
    input_text = re.sub(RE_MULTIPLE_WHITESPACES_BETWEEN_DIGITS, r'\1\2', input_text)
    return input_text


def has_numbers(input_text: str) -> bool:
    return any(char.isdigit() for char in input_text)


def _is_valid_budget(text) -> bool:
    only_digits = re.sub("[^0-9]", "", text)
    if only_digits.startswith("0"):
        return False
    for blacklisted_char in BLACKLISTED_CHARS_IN_RESULT:
        if blacklisted_char in text:
            return False
    return True


def _get_budgetary_values_from_text(text: str) -> List[Tuple[str, str]]:
    preprocessed_context = preprocess_input_text(text)
    tokenized_context = preprocessed_context.split(" ")
    monetary_values = []
    context_length = 20
    for ind, element in enumerate(tokenized_context):
        if has_numbers(element):
            for cur in CUSTOM_CURRENCY_CODES:
                if cur in element:
                    processed_tok = re.sub(r'\W+', '', element)  # Remove special characters
                    processed_tok = re.sub(r'\d+', '', processed_tok)  # Remove digits
                    if processed_tok == cur:
                        if _is_valid_budget(element):
                            context_index_min = max(0, ind-context_length)
                            context_index_max = min(len(tokenized_context), ind+context_length)
                            context = " ".join(tokenized_context[context_index_min:context_index_max])
                            monetary_values.append([element, context])
                else:
                    prev_index = ind-1
                    if prev_index >= 0:
                        prev_token = tokenized_context[prev_index]
                        processed_tok = re.sub(r'\W+','', prev_token)
                        if cur in prev_token and (prev_token.endswith(cur) or prev_token.endswith(f"{cur}.")) and processed_tok == cur:
                            value_budget = f"{prev_token} {element}"
                            if _is_valid_budget(value_budget):
                                context_index_min = max(0, ind-context_length)
                                context_index_max = min(len(tokenized_context), ind+context_length)
                                context = " ".join(tokenized_context[context_index_min:context_index_max])
                                monetary_values.append([value_budget, context])
                    next_index = ind+1
                    if next_index < len(tokenized_context):
                        next_token = tokenized_context[next_index]
                        processed_tok = re.sub(r'\W+','', next_token)
                        if cur in next_token and (next_token.startswith(cur) or next_token.startswith(f".{cur}")) and processed_tok == cur:
                            value_budget = f"{element} {next_token}"
                            if _is_valid_budget(value_budget):
                                context_index_min = max(0, ind-context_length)
                                context_index_max = min(len(tokenized_context), ind+context_length)
                                context = " ".join(tokenized_context[context_index_min:context_index_max])
                                monetary_values.append([value_budget, context])
            for cur in CUSTOM_CURRENCY_SYMBOLS:
                if cur in element:
                    processed_tok = re.sub(r'\d+','', element)
                    processed_tok = processed_tok.replace(".", "")
                    processed_tok = processed_tok.replace(",", "")
                    if processed_tok == cur:
                        value_budget = f"{element}"
                        if _is_valid_budget(value_budget):
                            context_index_min = max(0, ind-context_length)
                            context_index_max = min(len(tokenized_context), ind+context_length)
                            context = " ".join(tokenized_context[context_index_min:context_index_max])
                            monetary_values.append([value_budget, context])
                else:
                    prev_index = ind-1
                    if prev_index >= 0:
                        prev_token = tokenized_context[prev_index]
                        processed_tok = prev_token.replace(".", "")
                        processed_tok = processed_tok.replace(",", "")
                        if cur in prev_token and prev_token.endswith(cur):
                            value_budget = f"{prev_token} {element}"
                            if _is_valid_budget(value_budget):
                                context_index_min = max(0, ind-context_length)
                                context_index_max = min(len(tokenized_context), ind+context_length)
                                context = " ".join(tokenized_context[context_index_min:context_index_max])
                                monetary_values.append([value_budget, context])
                    next_index = ind+1
                    if next_index < len(tokenized_context):
                        next_token = tokenized_context[next_index]
                        processed_tok = next_token.replace(".", "")
                        processed_tok = processed_tok.replace(",", "")
                        if cur in next_token and next_token.startswith(cur):
                            value_budget = f"{element} {next_token}"
                            if _is_valid_budget(value_budget):
                                context_index_min = max(0, ind-context_length)
                                context_index_max = min(len(tokenized_context), ind+context_length)
                                context = " ".join(tokenized_context[context_index_min:context_index_max])
                                monetary_values.append([value_budget, context])

    return monetary_values


class ModelService(object):
    model = None

    @classmethod
    def get_model(cls):
        """Get the model object for this instance, loading it if it's not already loaded."""
        if cls.model is None:
            cls.model = spacy.load("/opt/ml/model/is_contract_budget")
        return cls.model

    @classmethod
    def predict(cls, input):
        """For the input, do the predictions and return them."""
        clf = cls.get_model()
        return clf.predict(input)


def predict(json_input):
    """
    Prediction given the request input
    :param json_input: [dict], request input
    :return: [dict], prediction
    """

    print(f'Extracting fields "text" from JSON input')
    text = json_input.get("text", "")
    if not text:
        raise Exception("No text field provided")
    # print(f"Received: {text}")
    preprocessed_text = preprocess_input_text(text)
    # print(f"Preprocessed text: {preprocessed_text}")

    results = _get_budgetary_values_from_text(preprocessed_text)
    print(f"Extracting budgetary values from text: {results}")

    results_with_placeholder = []
    for element in results:
        placeholder_in_result = element[1].replace(element[0], "extractedbudgetaryvalue")
        results_with_placeholder.append([element[0], placeholder_in_result])
    print(f"Results with placeholder status: {results_with_placeholder}")

    filtered_results = []
    print("=============================")
    print("All detected budgetary values:")
    for index, result in enumerate(results_with_placeholder):
        nlp = ModelService.get_model()
        probability = nlp(result[1]).cats["Contract"]
        print(f"Probability: {probability} \t Extracted: {result[0]} \t Context: {result[1]}")
        if probability >= 0.5:
            filtered_results.append({"probability": probability, "extracted_value": result[0],
                                     "context": results[index][1]})

    print("=============================")
    print("After applying threshold:")
    print(filtered_results)

    return filtered_results
